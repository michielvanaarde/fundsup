﻿--2019/05/17
--Add short_desription to tbl_cause
 --alter table tbl_cause add short_description varchar(50)  null

 --2019/05/23
 --Added is published to causes
 --alter table tbl_cause add is_published bit null

 --2019/05/29
 --Added social media handles to tbl_donor
alter table tbl_donor add facebook_handle_url varchar(250) null
alter table tbl_donor add twitter_handle varchar(100) null
alter table tbl_donor add instagram_handle_username varchar(150) null
alter table tbl_donor add linkedin_handle_url varchar(150) null