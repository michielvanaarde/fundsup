﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FundsupWebApplication.Models.ViewModels
{
    public class PaymentViewModel
    {
        [Required]
        public long DonorKey { get; set; }
        public Entity.CauseEntity Cause { get; set; }
        [Required]
        public long TransactionKey { get; set; }
        [Required]
        public string CreditCardType { get; set; }
        [Required]
        public string CreditcardHolderName { get; set; }
        [Required]
        public string CreditCardNumber { get; set; }
        [Required]
        public int ExpiryMonth { get; set; }
        [Required]
        public int ExpiryYear { get; set; }
        [Required]
        public string CCV { get; set; }

        public SelectList CreditCardTypes { get; set; }
        public SelectList ExpiryMonths { get; set; }
        public SelectList ExpiryYears { get; set; }
    }
}