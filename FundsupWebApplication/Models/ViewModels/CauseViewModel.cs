﻿using FundsupWebApplication.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Models.ViewModels
{
    public class CauseViewModel
    {
        public long DonorKey { get; set; }
        public CauseEntity Cause { get; set; }
        
        public MilestoneEntity NewMilestone { get; set; }
        public List<MilestoneEntity> Milestones { get; set; }


    }
}