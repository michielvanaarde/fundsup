﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Models.ViewModels
{
    public class SearchDonateViewModel
    {
        public string SearchText { get; set; }
        public long DonorKey { get; set; }
        public List<Entity.CauseEntity> Causes { get; set; }
    }
}