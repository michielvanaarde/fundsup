﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Models.DBContext
{
    public class CauseClass
    {
        public List<Entity.CauseEntity> GetDonorCauses(long donorKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsUp = new FundsupEntities())
            {
                return (from cause in dbFundsUp.tbl_donor_cause
                        where cause.donor_key == donorKey
                        select new Entity.CauseEntity
                        {
                            Description = cause.tbl_cause.description,
                            ShortDescription = cause.tbl_cause.short_description,
                            Key = cause.cause_key,
                            Name = cause.tbl_cause.name,
                            Amount = cause.tbl_cause.amount.HasValue ? cause.tbl_cause.amount.Value : 0,
                            IsPublished = cause.tbl_cause.is_published.HasValue ? cause.tbl_cause.is_published.Value : false,
                        }).ToList();
            }
        }

        public Entity.CauseEntity GetDonorCause(long donorCauseKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsUp = new FundsupEntities())
            {
                return (from cause in dbFundsUp.tbl_donor_cause
                        where cause.donor_cause_key == donorCauseKey
                        select new Entity.CauseEntity
                        {
                            Description = cause.tbl_cause.description,
                            Key = cause.tbl_cause.cause_key,
                            Name = cause.tbl_cause.name,
                            ShortDescription = cause.tbl_cause.short_description,
                            Amount = cause.tbl_cause.amount.HasValue ? cause.tbl_cause.amount.Value : 0,
                            IsPublished = cause.tbl_cause.is_published.HasValue ? cause.tbl_cause.is_published.Value : false,
                        }).FirstOrDefault();
            }
        }

        public Entity.CauseEntity GetCause(long causeKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsUp = new FundsupEntities())
            {
                return (from cause in dbFundsUp.tbl_cause
                        where cause.cause_key == causeKey
                        select new Entity.CauseEntity
                        {
                            Description = cause.description,
                            Key = cause.cause_key,
                            Name = cause.name,
                            ShortDescription = cause.short_description,
                            Amount = cause.amount.HasValue ? cause.amount.Value : 0,
                            IsPublished = cause.is_published.HasValue ? cause.is_published.Value : false,
                        }).FirstOrDefault();
            }
        }

        public List<Entity.CauseEntity> GetFilteredCauses(string prefix)
        {
            using (Models.DBContext.FundsupEntities dbFundsUp = new FundsupEntities())
            {
                return (from cause in dbFundsUp.tbl_cause
                        where (cause.name.ToUpper().Contains(prefix.ToUpper())
                        || cause.short_description.ToUpper().Contains(prefix.ToUpper()))
                        && (cause.is_published.HasValue && cause.is_published.Value)
                        select new Entity.CauseEntity
                        {
                            Description = cause.description,
                            Key = cause.cause_key,
                            Name = cause.name,
                            ShortDescription = cause.short_description,
                            Amount = cause.amount.HasValue ? cause.amount.Value : 0,
                        }).Distinct().ToList();
            }
        }


        public Entity.CauseEntity EditDonorCause(Entity.CauseEntity cause, long donorKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsup = new Models.DBContext.FundsupEntities())
            {
                if (cause.Key > 0)
                {
                    //edit
                    var dbCause = dbFundsup.tbl_cause.Find(cause.Key);
                    if (dbCause != null)
                    {
                        dbCause.name = cause.Name;
                        dbCause.description = cause.Description;
                        dbCause.amount = cause.Amount;
                        dbCause.short_description = cause.ShortDescription;
                        dbFundsup.Entry(dbCause).State = System.Data.Entity.EntityState.Modified;
                    }
                    dbFundsup.SaveChanges();
                }
                else
                {
                    //new
                    var dbCause = dbFundsup.tbl_donor_cause.Add(new tbl_donor_cause
                    {
                         donor_key = donorKey,
                         tbl_cause = new tbl_cause
                         {
                             name = cause.Name,
                             description = cause.Description,
                             amount = cause.Amount,
                             short_description = cause.ShortDescription,
                         }
                    });
                    dbFundsup.SaveChanges();
                    cause.Key = dbCause.cause_key;
                }
                return cause;
            }
        }

        public bool PublishProfile(long causeKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsup = new Models.DBContext.FundsupEntities())
            {
                var dbCause = dbFundsup.tbl_cause.Find(causeKey);
                if (dbCause != null)
                {
                    dbCause.is_published = dbCause.is_published.HasValue ? !dbCause.is_published.Value : true;
                    dbFundsup.SaveChanges();
                    return dbCause.is_published.Value;
                }
            }

            return false;
        }
    }
}