﻿using FundsupWebApplication.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Models.DBContext
{
    public class DonationClass
    {
        public List<CauseEntity> GetDonorDonations(long donorKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsup = new Models.DBContext.FundsupEntities())
            {
                return(from transactionDetail in dbFundsup.tbl_payment_transaction_detail
                              where transactionDetail.tbl_payment_transaction.donor_key == donorKey
                              select new CauseEntity
                              {
                                   Key = transactionDetail.tbl_payment_transaction.cause_key,
                                   Amount = transactionDetail.amount,
                                   Name = transactionDetail.tbl_payment_transaction.tbl_cause.name,
                              }).ToList();

            }
        }


        public CauseEntity GetDonationAmount(long transactionKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsup = new Models.DBContext.FundsupEntities())
            {
                return (from transactionDetail in dbFundsup.tbl_payment_transaction_detail
                        where transactionDetail.payment_transaction_key == transactionKey
                        select new CauseEntity
                        {
                            Amount = transactionDetail.amount,
                            Name = transactionDetail.tbl_payment_transaction.tbl_cause.name,
                        }).ToList().GroupBy(l => l.Name)
                        .Select(cause => new CauseEntity
                        {
                            Name = cause.First().Name,
                            Amount = cause.Sum(c => c.Amount),
                        }).FirstOrDefault();
            }
        }
    }
}