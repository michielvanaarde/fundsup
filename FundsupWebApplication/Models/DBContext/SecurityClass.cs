﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Models.DBContext
{
    public class SecurityClass
    {
        public long RegisterDonor (RegisterViewModel model, string userId)
        {
            using (Models.DBContext.FundsupEntities dbFundsUp = new FundsupEntities())
            {
                var donor = dbFundsUp.tbl_donor.Add(new tbl_donor
                {
                    cellphone_number = model.CellphoneNumber,
                    first_name = model.FirstName,
                    last_name = model.LastName,
                    user_id = userId,
                });

                dbFundsUp.SaveChanges();

                return donor.donor_key;
            }
        }

        public long GetDonorKeyFromEmail(string email)
        {
            using (Models.DBContext.FundsupEntities dbFundsUp = new FundsupEntities())
            {
                return (from user in dbFundsUp.AspNetUsers
                              join donor in dbFundsUp.tbl_donor on user.Id equals donor.user_id
                              where user.Email == email
                              select donor.donor_key).FirstOrDefault();
                                                           
            }
        }

    }

}