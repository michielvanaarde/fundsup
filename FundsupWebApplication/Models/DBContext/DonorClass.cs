﻿using FundsupWebApplication.Entity;
using FundsupWebApplication.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Models.DBContext
{
    public class DonorClass
    {
        public DonorEntity GetDonor(long donorKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsup = new Models.DBContext.FundsupEntities())
            {
                var aDonor = (from donor in dbFundsup.tbl_donor
                              join user in dbFundsup.AspNetUsers on donor.user_id equals user.Id
                              where donor.donor_key == donorKey
                              select new DonorEntity
                              {
                                  CellphoneNumber = donor.cellphone_number,
                                  Email = user.Email,
                                  FirstName = donor.first_name,
                                  Key = donor.donor_key,
                                  LastName = donor.last_name,
                                  UserId = donor.user_id,
                                  Image = donor.profile_pic,
                                  FacebookHandleUrl = string.IsNullOrEmpty(donor.facebook_handle_url) ? string.Empty : donor.facebook_handle_url,
                                  InstagramHandleUsername = string.IsNullOrEmpty(donor.instagram_handle_username) ? string.Empty : donor.instagram_handle_username,
                                  TwitterHandle = string.IsNullOrEmpty(donor.twitter_handle) ? string.Empty : donor.twitter_handle,
                                  LinkedinHandleUrl = string.IsNullOrEmpty(donor.linkedin_handle_url) ? string.Empty : donor.linkedin_handle_url,
                              }).FirstOrDefault();

                if (aDonor != null)
                {
                    aDonor.Causes = new Models.DBContext.CauseClass().GetDonorCauses(donorKey);
                    aDonor.Donations = new Models.DBContext.DonationClass().GetDonorDonations(donorKey);
                    return aDonor;
                }
                else
                    return new DonorEntity { Key = donorKey };
            }
        }
        public Entity.DonorEntity EditDonor(Entity.DonorEntity donor)
        {
            using (Models.DBContext.FundsupEntities dbFundsup = new Models.DBContext.FundsupEntities())
            {
                if (donor.Key > 0)
                {
                    //edit
                    var dbDonor = dbFundsup.tbl_donor.Find(donor.Key);
                    if (dbDonor != null)
                    {
                        dbDonor.cellphone_number = donor.CellphoneNumber;
                        dbDonor.first_name = donor.FirstName;
                        dbDonor.last_name = donor.LastName;
                        dbDonor.facebook_handle_url = donor.FacebookHandleUrl;
                        dbDonor.instagram_handle_username = donor.InstagramHandleUsername;
                        dbDonor.twitter_handle = donor.TwitterHandle;
                        dbDonor.linkedin_handle_url = donor.LinkedinHandleUrl;
                        dbFundsup.Entry(dbDonor).State = System.Data.Entity.EntityState.Modified;
                    }

                }
                else
                {
                    var dbDonor = new tbl_donor
                    {
                        cellphone_number = donor.CellphoneNumber,
                        first_name = donor.FirstName,
                        last_name = donor.LastName,
                    };

                    if(!string.IsNullOrEmpty(donor.FacebookHandleUrl))
                        dbDonor.facebook_handle_url = donor.FacebookHandleUrl;
                    if (!string.IsNullOrEmpty(donor.InstagramHandleUsername))
                        dbDonor.instagram_handle_username = donor.InstagramHandleUsername;
                    if (!string.IsNullOrEmpty(donor.TwitterHandle))
                        dbDonor.twitter_handle = donor.TwitterHandle;
                    if (!string.IsNullOrEmpty(donor.LinkedinHandleUrl))
                        dbDonor.linkedin_handle_url = donor.LinkedinHandleUrl;

                    dbFundsup.tbl_donor.Add(dbDonor);
                }

                dbFundsup.SaveChanges();

                return donor;
            }
        }
        public long Donate(List<Entity.MilestoneEntity> donations, decimal totalAmountDonated,  long donorKey, long causeKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsup = new Models.DBContext.FundsupEntities())
            {
                    List<tbl_payment_transaction_detail> details = new List<tbl_payment_transaction_detail>();
                    if (donations != null && donations.Count > 0)
                    {
                        foreach (var detail in donations)
                        {
                            if (detail.Amount > 0)
                            {
                                details.Add(new tbl_payment_transaction_detail
                                {
                                    amount = (decimal)detail.Amount,
                                    milestone_key = detail.Key,
                                });
                            }
                        }
                    }

                    if(totalAmountDonated > 0)
                    {
                        details.Add(new tbl_payment_transaction_detail
                        {
                            amount = totalAmountDonated,
                        });
                    }

                    if (details != null && details.Count > 0)
                    {
                        var dbTransaction = dbFundsup.tbl_payment_transaction.Add(new tbl_payment_transaction
                        {
                            approved = false,
                            cause_key = causeKey,
                            donor_key = donorKey,
                            transaction_date = DateTime.Now,
                            tbl_payment_transaction_detail = details,
                        });
                        dbFundsup.SaveChanges();

                    return dbTransaction.payment_transaction_key;
                }

                return 0;
                
            }
        }

        public void UploadImageInDataBase(HttpPostedFileBase file, ContentViewModel model)
        {
            model.Image = ConvertToBytes(file);

            using (Models.DBContext.FundsupEntities dbFundsup = new Models.DBContext.FundsupEntities())
            {
                var dbDonor = dbFundsup.tbl_donor.Find(model.DonorKey);
                if(dbDonor != null)
                {
                    dbDonor.profile_pic = model.Image;
                    dbFundsup.Entry(dbDonor).State = System.Data.Entity.EntityState.Modified;
                    dbFundsup.SaveChanges();
                        
                }
            }
        }
        public byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(image.InputStream);
            imageBytes = reader.ReadBytes((int)image.ContentLength);
            return imageBytes;
        }


    }
}