//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FundsupWebApplication.Models.DBContext
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_payment_transaction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_payment_transaction()
        {
            this.tbl_payment_transaction_detail = new HashSet<tbl_payment_transaction_detail>();
        }
    
        public long payment_transaction_key { get; set; }
        public System.DateTime transaction_date { get; set; }
        public long donor_key { get; set; }
        public long cause_key { get; set; }
        public bool approved { get; set; }
    
        public virtual tbl_donor tbl_donor { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_payment_transaction_detail> tbl_payment_transaction_detail { get; set; }
        public virtual tbl_cause tbl_cause { get; set; }
    }
}
