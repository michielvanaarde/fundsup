﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Models.DBContext
{
    public class MilestoneClass
    {
        public List<Entity.MilestoneEntity> GetMilestonesPerCause(long causeKey)
        {
            using (Models.DBContext.FundsupEntities dbFundsUp = new FundsupEntities())
            {
                return (from milestone in dbFundsUp.tbl_milestone
                        where milestone.cause_key == causeKey
                        select new Entity.MilestoneEntity
                        {
                            Amount = (double)milestone.funds_required,
                            CauseKey = milestone.cause_key,
                            EndDate = milestone.end_date,
                            StartDate = milestone.start_date,
                            Name = milestone.name,
                            Key = milestone.milestone_key,
                        }).ToList();
            }
        }

        public void EditMilestone(Entity.MilestoneEntity milestone)
        {
            using (Models.DBContext.FundsupEntities dbFundsup = new Models.DBContext.FundsupEntities())
            {
                if (!String.IsNullOrWhiteSpace(milestone.Name) && milestone.Amount > 0)
                {
                    if (milestone.Key > 0)
                    {
                        //edit
                        var dbMilestone = dbFundsup.tbl_milestone.Find(milestone.Key);
                        if (dbMilestone != null)
                        {
                            dbMilestone.name = milestone.Name;
                            dbMilestone.start_date = milestone.StartDate;
                            dbMilestone.end_date = milestone.EndDate;
                            dbMilestone.funds_required = (decimal)milestone.Amount;

                            dbFundsup.Entry(dbMilestone).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                    else
                    {
                        //new
                        dbFundsup.tbl_milestone.Add(new tbl_milestone
                        {
                            cause_key = milestone.CauseKey,
                            end_date = milestone.EndDate,
                            funds_required = (decimal)milestone.Amount,
                            name = milestone.Name,
                            start_date = milestone.StartDate,
                        });
                    }

                    dbFundsup.SaveChanges();
                }
            }
        }
    }
}