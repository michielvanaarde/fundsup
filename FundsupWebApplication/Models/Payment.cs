﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Models
{
    public class Payment
    {
        public string MerchantID { get; set; }

        public string TransactionID { get; set; }
        //public string PreviousTransactionID string
        //public string TransactionDateTime datetime
        //public string SaleReconID string
        //public string ReconID string
        public long Amount { get; set; }
        public string CurrencyCode { get; set; }
        //public string BudgetPeriod string
        //public string OrderNumber string
        //public string AuthCode string
        public string PaymentService { get; set; }
        //public string Card BankCard
        //public string BankAccount BankAccountType
        //public string FirstName string
        //public string LastName string
        //BillingAddress  Address
        //ShippingAddress Address
        //Email   string
        //MobilePhone string
        //HomePhone   string
        //WorkPhone   string
        //PAResPayload    string
        //TerminalID  string
        //CardAcceptorName    string

    }

    public enum PaymentService  {
        CardNotPresent , //A real-time card not present transaction.
        CardNotPresentRecurring, //A recurring card not present transaction
        CardNotPresentMoto, //A mail order / telephone order transaction.
        CardNotPresentMotoRecurring, //A recurring mail order / telephone order transaction
        CardNotPresentBatch, //A batch card not present transaction.
        EftSecure, //A EFTSecure payment transaction.
        MasterPass, //A MasterPass payment transaction.
        Sid, //A SID payment transaction.
        SnapScan, //A SnapScan payment transaction.
        WiCode, //A WiCode payment transaction.
        Zapper, //A Zapper payment transaction.
    }
}




//ELEMENT | TYPE | OCCURS | DESCRIPTION
//MessageHeader |  | 0..1 | 
//MessageDateTime | datetime | 0..1 | The message timestamp.
//MessageID | string | 0..1 | A unique identifier to identify the message.
//Channel | string | 0..1 | An identifier to identify the source of the message.
//PaymentRequest |  | 1 | 
//MerchantID | string | 1 | A unique identifier to identify the merchant.Also known as Merchant GUID.
//TransactionID | string | 0..1 | A unique identifier to identify the transaction.
//PreviousTransactionID | string | 0..1 | A unique identifier to identify a preceding transaction (Secure3DLookup).
//TransactionDateTime | datetime | 0..1 | The transaction timestamp.
//SaleReconID | string | 0..1 | A unique identifier for reconciliation purposes.
//ReconID | string | 0..1 | A unique identifier for reconciliation purposes.
//Amount | long | 1 | The authorization amount in the lowest monetary unit.
//CurrencyCode | string | 0..1 | The currency code according to the ISO 4217 standard.
//BudgetPeriod | string | 0..1 | The payment payback period. Available for bank cards with a budget facility.
//OrderNumber | string | 0..1 | The transaction order or reference number.
//AuthCode | string | 0..1 | The authorization code received from the payment service.
//PaymentService | PaymentServiceType | 1 | The target payment service.
//Card | BankCard | 0..1 | The card details.
//BankAccount | BankAccountType | 0..1 | The card bank account type.
//FirstName | string | 0..1 | The customer first name.
//LastName | string | 0..1 | The customer last name.
//BillingAddress | Address | 0..1 | The billing address.
//ShippingAddress | Address | 0..1 | The shipping address.
//Email | string | 0..1 | The customer email.
//MobilePhone | string | 0..1 | The customer mobile phone number.
//HomePhone | string | 0..1 | The customer home phone number.
//WorkPhone | string | 0..1 | The customer work phone number.
//PAResPayload | string | 0..1 | The PARes Payload obtained through the 3D Secure authentication process.
//TerminalID | string | 0..1 | The TerminalID to be passed in Postilion field 41 for Card transactions. If left blank, the configured value will be used.
