﻿using FundsupWebApplication.API;
using FundsupWebApplication.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FundsupWebApplication.Controllers
{
    //[Authorize]
    public class DonorController : Controller
    {
        // GET: Doner            ModelState.Clear();            ModelState.Clear();
        public ActionResult Profile(long donorKey)
        {
            var donor = new Models.DBContext.DonorClass().GetDonor(donorKey);
            if (donor == null)
                throw new ApplicationException("Donor Key cannot be null");
            
            return View(donor);
        }

        public ActionResult UploadProfilePicture(long donorKey)
        {
            Models.ViewModels.ContentViewModel model = new Models.ViewModels.ContentViewModel
            {
                DonorKey = donorKey,
            };
            return View();
        }

        [HttpPost]
        public ActionResult UploadProfilePicture(Models.ViewModels.ContentViewModel model)
        {
            HttpPostedFileBase file = Request.Files["ImageData"];

            if(ModelState.IsValid)
            { 
                //TODO add some more security around image add. 
                new Models.DBContext.DonorClass().UploadImageInDataBase(file, model);
                return RedirectToAction("Profile", new { donorKey = model.DonorKey });
            }

            return View(model);
        }

        #region EditDonor
        [HttpGet]
        public ActionResult EditDonor(long donorKey)
        {
            var model = new Models.DBContext.DonorClass().GetDonor(donorKey);
            if (model == null)
                throw new ApplicationException("Donor Key cannot be null");

            return View(model);
        }

        [HttpPost]
        public ActionResult EditDonor(DonorEntity model)
        {
            if(ModelState.IsValid)
            {
                new Models.DBContext.DonorClass().EditDonor(model);
                return RedirectToAction("Profile", new { donorKey = model.Key });
            }

            if (model == null)
                throw new ApplicationException("Donor Key cannot be null");

            return View(model);
        }
        #endregion

        #region EditCause
        [HttpGet]
        public ActionResult EditCause(long? causeKey, long donorKey)
        {
            var model = new Models.ViewModels.CauseViewModel
            { 
                DonorKey = donorKey,
                Cause = causeKey.HasValue && causeKey.Value > 0 ? new Models.DBContext.CauseClass().GetCause(causeKey.Value) : new CauseEntity(),
                Milestones = new List<MilestoneEntity>(),
                NewMilestone = new MilestoneEntity
                {
                     Amount = 0,
                     StartDate = DateTime.Now,
                     EndDate = DateTime.Now,
                     Name = string.Empty,
                },
            };

            if (causeKey.HasValue && causeKey.Value > 0)
            {
                model.NewMilestone.CauseKey = causeKey.Value;
                model.Milestones = new Models.DBContext.MilestoneClass().GetMilestonesPerCause(causeKey.Value);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult EditCause(Models.ViewModels.CauseViewModel model)
        {
            if (model.Milestones == null)
                model.Milestones = new List<MilestoneEntity>();

            if (ModelState.IsValid)
            {

                //edit cause details
                var cause = new Models.DBContext.CauseClass().EditDonorCause(model.Cause, model.DonorKey);

                if (model.NewMilestone.CauseKey == 0)
                    model.NewMilestone.CauseKey = cause.Key;

                //add new milestone
                new Models.DBContext.MilestoneClass().EditMilestone(model.NewMilestone);

                //edit existing milestones
                
                foreach(var milestone in model.Milestones)
                {
                    new Models.DBContext.MilestoneClass().EditMilestone(milestone);
                }

                //return RedirectToAction("Profile", new { donorKey = model.DonorKey });
            }

            ModelState.Clear();
            model.NewMilestone = new MilestoneEntity
            {
                EndDate = DateTime.Now,
                StartDate = DateTime.Now,
                Amount = 0,
            };

            if (model == null)
                throw new ApplicationException("Cause Key cannot be null");

            model.Milestones = new Models.DBContext.MilestoneClass().GetMilestonesPerCause(model.Cause.Key);
            model.NewMilestone = new MilestoneEntity
            {
                Amount = 0,
                StartDate = DateTime.Now,
                EndDate = DateTime.Now,
                Name = string.Empty,
            };
            return View(model);
        }
        #endregion

        #region SearchDonate
        [HttpGet]
        public ActionResult SearchDonate(long donorKey)
        {
            Models.ViewModels.SearchDonateViewModel model = new Models.ViewModels.SearchDonateViewModel
            {
                DonorKey = donorKey,
                Causes = new List<CauseEntity>(),
                SearchText = string.Empty,
            };
            
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchDonate(Models.ViewModels.SearchDonateViewModel model)
        {

            if (String.IsNullOrEmpty(model.SearchText))
                model.Causes = new List<CauseEntity>();
            else
                model.Causes = new Models.DBContext.CauseClass().GetFilteredCauses(model.SearchText);

            return View(model);
        }

        #endregion

        #region EditDonation
        [HttpGet]
        public ActionResult EditDonation(long? causeKey, long donorKey)
        {
            var model = new Models.ViewModels.DonateViewModel
            {
                DonorKey = donorKey,
                Cause = causeKey.HasValue && causeKey.Value > 0 ? new Models.DBContext.CauseClass().GetCause(causeKey.Value) : new CauseEntity(),
                Milestones = new List<MilestoneEntity>(),
            };

            if (causeKey.HasValue && causeKey.Value > 0)
            {
                model.Milestones = new Models.DBContext.MilestoneClass().GetMilestonesPerCause(causeKey.Value);
                model.Milestones.ForEach(s => s.Amount = 0);
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult EditDonation(Models.ViewModels.DonateViewModel model)
        {
            if (model.Milestones == null)
                model.Milestones = new List<MilestoneEntity>();

            if (ModelState.IsValid)
            {
               long transactionKey = new Models.DBContext.DonorClass().Donate(model.Milestones, model.Cause.Amount, model.DonorKey, model.Cause.Key);
                if (transactionKey != 0)
                    return RedirectToAction("BankPaymentProcess", new { donorKey = model.DonorKey, causeKey = model.Cause.Key, transactionKey = transactionKey });
                else
                    ModelState.AddModelError("transactionKey is 0", "Transaction was not processes. Please make sure the donation amounts are greater than 0.");
            }

            if (model == null)
                throw new ApplicationException("Cause Key cannot be null");

            model.Milestones = new Models.DBContext.MilestoneClass().GetMilestonesPerCause(model.Cause.Key);

            return View(model);
        }
        #endregion

        #region BankPaymentProcess
        [HttpGet]
        public ActionResult BankPaymentProcess(long donorKey, long causeKey, long transactionKey)
        {
            Models.ViewModels.PaymentViewModel model = new Models.ViewModels.PaymentViewModel
            {
                DonorKey = donorKey,
                Cause = new Models.DBContext.DonationClass().GetDonationAmount(transactionKey),
                TransactionKey = transactionKey,
                CreditCardTypes = new API.EcentricWebAPI().GetCreditCardTypes() ,
                ExpiryMonths = new API.EcentricWebAPI().GetCreditCardMonths(),
                ExpiryYears = new API.EcentricWebAPI().GetCreditCardYears(),
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult PublishProfile(long causeKey, long donorKey)
        {
            new Models.DBContext.CauseClass().PublishProfile(causeKey);
            return RedirectToAction("Profile", new { causeKey = causeKey, donorKey = donorKey });
        }


        [HttpPost]
        public ActionResult BankPaymentProcess(Models.ViewModels.PaymentViewModel model)
        {
            ModelState.Remove("Cause.ShortDescription");
            ModelState.Remove("Cause.Description");

            if (ModelState.IsValid)
            {
                var onceOffRequest = new PayFastRequest(Sessions.SiteSettings.SandboxPassphrase);

                // Merchant Details
                onceOffRequest.merchant_id = Sessions.SiteSettings.SandboxMerchantID;
                onceOffRequest.merchant_key = Sessions.SiteSettings.SandboxMerchantKey;
                onceOffRequest.return_url = Url.Action("ReturnUrl", new { } );
                onceOffRequest.cancel_url = Url.Action("CancelUrl", new { });
                onceOffRequest.notify_url = Url.Action("NotifyUrl", new { });

                // Buyer Details
                onceOffRequest.email_address = "sbtu01@payfast.co.za";

                // Transaction Details
                onceOffRequest.m_payment_id = "8d00bf49-e979-4004-228c-08d452b86380";
                onceOffRequest.amount = 130;
                onceOffRequest.item_name = "Once off option";
                onceOffRequest.item_description = "Some details about the once off payment";

                // Transaction Options
                onceOffRequest.email_confirmation = true;
                onceOffRequest.confirmation_address = "sbtu01@payfast.co.za";
                onceOffRequest
                var redirectUrl = $"{Sessions.SiteSettings.ProcessUrl}{onceOffRequest.ToString()}";

                return Redirect(redirectUrl);

            }

            model.CreditCardTypes = new API.EcentricWebAPI().GetCreditCardTypes();
            model.ExpiryMonths = new API.EcentricWebAPI().GetCreditCardMonths();
            model.ExpiryYears = new API.EcentricWebAPI().GetCreditCardYears();

            return View(model);
        }

        public ActionResult ReturnUrl()
        {
            return View();
        }

        public ActionResult CancelUrl()
        {
            return View();
        }
        public ActionResult NotifyUrl()
        {
            return View();
        }

        #endregion

        public JsonResult GetCauses(string prefix)
        {
            return Json(new Models.DBContext.CauseClass().GetFilteredCauses(prefix), JsonRequestBehavior.AllowGet);
        }

        // GET: Doner/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Doner/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Doner/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Doner/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Doner/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Doner/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Doner/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
