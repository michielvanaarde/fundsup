﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Entity
{
    public class DonorEntity
    {
        public long Key { get; set; }

        [DisplayName("First name")]
        public string FirstName { get; set; }

        [DisplayName("Lastname")]
        public string LastName { get; set; }

        [DisplayName("Email")]
        public string Email { get; set; }

        [DisplayName("Cellphone")]
        public string CellphoneNumber { get; set; }

        public string UserId { get; set; }

        public byte[] Image { get; set; }
        public string FacebookHandleUrl { get; set; } 
        public string TwitterHandle { get; set; }
        public string InstagramHandleUsername { get; set; }
        public string LinkedinHandleUrl { get; set; } 
        public List<CauseEntity> Causes { get; set; }

        public List<CauseEntity> Donations { get; set; }

        
    }
}