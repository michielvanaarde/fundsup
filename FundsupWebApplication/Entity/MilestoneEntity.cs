﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Entity
{
    public class MilestoneEntity
    {
        public long Key { get; set; }
        public long CauseKey { get; set; }
        public string Name { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime StartDate { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime EndDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:C}")]
        [Range(0, 2000000, ErrorMessage = "Cannot dontate more than 2 million.")]
        public double Amount { get; set; }
    }
}