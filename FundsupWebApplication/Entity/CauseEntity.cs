﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.Entity
{
    public class CauseEntity
    {
        public long Key { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string ShortDescription { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:C}")]
        [Range(0,2000000,ErrorMessage ="Cannot dontate more than 2 million.")]
        public decimal Amount { get; set; }
        public byte[] Image { get; set; }
        public List<MilestoneEntity> Milestones { get; set; }
        public bool IsPublished { get; set; }
    }
}