﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundsupWebApplication.API
{
    public enum BillingFrequency
    {
        Monthly = 3,
        Quarterly = 4,
        Biannual = 5,
        Annual = 6
    }
}