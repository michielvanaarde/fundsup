﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace FundsupWebApplication.API
{
    public class PayFast
    {
        private static HttpClient PayFastApiClient { get; set; }
        private const string MechantID = "14047197";
        private const string MerchantKey = "1zwxvulnhd2gu";
        private const string Username = "info@fundsup.co.za";
        private const string PassPhrase = "Forever2getherWifMyChinas";
        private const string PayFastApiVersion = "v1";
        private const string TestMode = "?testing=true";
        private const string CancelResourceUrl = "cancel";
        //buyerpass clientpass
        /// <summary>
        /// Initialise the API client
        /// </summary>
        public static void InitialiseClient()
        {
            PayFastApiClient = new HttpClient();
            PayFastApiClient.DefaultRequestHeaders.Accept.Clear();
            PayFastApiClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<string> PingPayFastAPI()
        {
            var defaultHeaders = new List<KeyValuePair<string, string>>();
            defaultHeaders.Add(new KeyValuePair<string, string>("merchant-id", MechantID));
            defaultHeaders.Add(new KeyValuePair<string, string>("timestamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss").ToString()));
            defaultHeaders.Add(new KeyValuePair<string, string>("version", PayFastApiVersion));
            string signature = GenerateSignature(PayFastApiClient, defaultHeaders.ToArray());

            PayFastApiClient.DefaultRequestHeaders.Add("merchant-id", MechantID);
            PayFastApiClient.DefaultRequestHeaders.Add("version", PayFastApiVersion);
            PayFastApiClient.DefaultRequestHeaders.Add("signature", signature);
            PayFastApiClient.DefaultRequestHeaders.Add("timestamp", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss").ToString());

            string pingUrl = $"https://api.payfast.co.za/ping";
            using (HttpResponseMessage response = await PayFastApiClient.GetAsync(pingUrl).ConfigureAwait(false))
            {
                if(response.IsSuccessStatusCode)
                {
                    string apiversion = await response.Content.ReadAsAsync<string>();
                    return apiversion;
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        private string GenerateSignature(HttpClient httpClient, params KeyValuePair<string, string>[] parameters)
        {
            var dictionary = new SortedDictionary<string, string>();

            foreach (var header in httpClient.DefaultRequestHeaders)
            {
                dictionary.Add(key: header.Key, value: header.Value.First());
            }

            foreach (var keyValuePair in parameters)
            {
                dictionary.Add(key: keyValuePair.Key, value: keyValuePair.Value);
            }

            if (!string.IsNullOrWhiteSpace(Sessions.SiteSettings.SandboxPassphrase))
            {
                dictionary.Add(key: "passphrase", value: Sessions.SiteSettings.SandboxPassphrase);
            }

            var stringBuilder = new StringBuilder();
            var last = dictionary.Last();

            foreach (var keyValuePair in dictionary)
            {
                stringBuilder.Append($"{ UrlEncode(keyValuePair.Key)}={ UrlEncode(keyValuePair.Value)}");

                if (keyValuePair.Key != last.Key)
                {
                    stringBuilder.Append("&");
                }
            }

            httpClient.DefaultRequestHeaders.Add(name: "signature", value: CreateHash(stringBuilder));

            if (parameters.Length > 0)
            {
                var jsonStringBuilder = new StringBuilder();
                jsonStringBuilder.Append("{");

                var lastParameter = parameters.Last();

                foreach (var keyValuePair in parameters)
                {
                    jsonStringBuilder.Append($"\"{keyValuePair.Key}\" : \"{keyValuePair.Value}\"");

                    if (lastParameter.Key != keyValuePair.Key)
                    {
                        jsonStringBuilder.Append(",");
                    }
                }

                jsonStringBuilder.Append("}");

                return jsonStringBuilder.ToString();
            }
            else
            {
                return null;
            }
        }

        private string CreateHash(StringBuilder input)
        {
            var inputStringBuilder = new StringBuilder(input.ToString());
            //if (!string.IsNullOrWhiteSpace(Sessions.SiteSettings.SandboxPassphrase))
            //{
            //    inputStringBuilder.Append($"passphrase={this.UrlEncode(Sessions.SiteSettings.SandboxPassphrase)}");
            //}

            var md5 = MD5.Create();

            var inputBytes = Encoding.ASCII.GetBytes(inputStringBuilder.ToString());

            var hash = md5.ComputeHash(inputBytes);

            var stringBuilder = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                stringBuilder.Append(hash[i].ToString("x2"));
            }

            return stringBuilder.ToString();
        }

        private string UrlEncode(string url)
        {
            Dictionary<string, string> convertPairs = new Dictionary<string, string>() { { "%", "%25" }, { "!", "%21" }, { "#", "%23" }, { " ", "+" },
            { "$", "%24" }, { "&", "%26" }, { "'", "%27" }, { "(", "%28" }, { ")", "%29" }, { "*", "%2A" }, { "+", "%2B" }, { ",", "%2C" },
            { "/", "%2F" }, { ":", "%3A" }, { ";", "%3B" }, { "=", "%3D" }, { "?", "%3F" }, { "@", "%40" }, { "[", "%5B" }, { "]", "%5D" } };

            var replaceRegex = new Regex(@"[%!# $&'()*+,/:;=?@\[\]]");
            MatchEvaluator matchEval = match => convertPairs[match.Value];
            string encoded = replaceRegex.Replace(url, matchEval);

            return encoded;
        }
    }
}