﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace FundsupWebApplication.API
{
    public class PayFastException : Exception
    {
        public PayFastException(HttpResponseMessage httpResponseMessage) : base(message: "Invalid Response. See The HttpResponseMessage Property For Details")
        {
            this.HttpResponseMessage = httpResponseMessage;
        }

        public HttpResponseMessage HttpResponseMessage { get; }
    }
}