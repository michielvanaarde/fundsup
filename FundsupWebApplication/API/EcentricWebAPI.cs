﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace FundsupWebApplication.API
{

    public class EcentricWebAPI
    {
        public System.Threading.Tasks.Task<EcentricPaymentService.PaymentResponse> MakeCreditCardPayment(long donorKey, long transactionKey, decimal amount, 
                                          string creditCardType, string creditcardHolderName, string creditCardNumber, int ExpiryMonth, int ExpiryYear, string CCV )
        {
            var donor = new Models.DBContext.DonorClass().GetDonor(donorKey);


            using (EcentricPaymentService.PaymentGatewayServiceClient service = new EcentricPaymentService.PaymentGatewayServiceClient())
            {
                var payment = service.PaymentAsync(new FundsupWebApplication.EcentricPaymentService.PaymentRequest
                {
                    Amount = (long)amount,
                    BankAccount = FundsupWebApplication.EcentricPaymentService.BankAccountType.Credit,
                    Card = new FundsupWebApplication.EcentricPaymentService.BankCard
                    {
                        CardAssociation = creditCardType,
                        CardholderName = creditcardHolderName,
                        CardNumber = creditCardNumber,
                        CardType = FundsupWebApplication.EcentricPaymentService.BankCardType.Credit,
                        ExpiryMonth = ExpiryMonth,
                        ExpiryYear = ExpiryYear,
                        SecurityCode = CCV,
                    },
                    CardAcceptorName = string.Empty,
                    CurrencyCode = "ZAR",
                    FirstName = donor.FirstName,
                    LastName = donor.LastName,
                    MerchantID = Sessions.SiteSettings.MerchantGUID,
                    OrderNumber = DateTime.Now.ToShortDateString() + "|" + transactionKey.ToString(),
                    PaymentService = FundsupWebApplication.EcentricPaymentService.PaymentServiceType.CardNotPresent,
                    TransactionDateTime = DateTime.Now,
                    TransactionID = DateTime.Now.ToShortDateString() + "|" + transactionKey.ToString(),
                });


                payment.Wait();

                if(payment.IsCompleted)
                {
                    var x = 1;
                }

                return payment;
            }

        }


        //public System.Threading.Tasks.Task<EcentricPaymentService.PaymentResponse> MakeCreditCardPaymentNotAsync(long donorKey, long transactionKey, decimal amount,
        //                               string creditCardType, string creditcardHolderName, string creditCardNumber, int ExpiryMonth, int ExpiryYear, string CCV)
        //{
        //    var donor = new Models.DBContext.DonorClass().GetDonor(donorKey);

        //    using (EcentricPaymentService.PaymentGatewayServiceClient service = new EcentricPaymentService.PaymentGatewayServiceClient())
        //    {
        //        var uniqueKey = DateTime.Now.ToShortDateString() + "|" + transactionKey.ToString();
        //        var payment = service.Payment( ref new EcentricPaymentService.MessageHeader {  }, Sessions.SiteSettings.MerchantGUID, ref uniqueKey,
        //                                string.Empty, DateTime.Now, string.Empty, string.Empty, amount, "ZAR", null, DateTime.Now.ToShortDateString() + "|" + transactionKey.ToString(),
        //                                string.Empty, EcentricPaymentService.PaymentServiceType.CardNotPresent, new EcentricPaymentService.BankCard
        //                                {
        //                                    CardAssociation = creditCardType,
        //                                    CardholderName = creditcardHolderName,
        //                                    CardNumber = creditCardNumber,
        //                                    CardType = FundsupWebApplication.EcentricPaymentService.BankCardType.Credit,
        //                                    ExpiryMonth = ExpiryMonth,
        //                                    ExpiryYear = ExpiryYear,
        //                                    SecurityCode = CCV,
        //                                }, EcentricPaymentService.BankAccountType.Credit, donor.FirstName, donor.LastName, new EcentricPaymentService.Address(), new EcentricPaymentService.Address(), donor.Email,
        //                                donor.CellphoneNumber, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, new EcentricPaymentService.ResponseDetail (), amount);
        //        return payment;
        //    }

        //}






        public SelectList GetCreditCardTypes()
        {
            var items = new List<SelectListItem>()
            {
                new SelectListItem{ Text="Mastercard", Value="Mastercard"},
                new SelectListItem{ Text="Visa", Value="Visa"},
            };

            return new SelectList(items, "Value", "Text");
        }

        public SelectList GetCreditCardMonths()
        {
            var items = new List<SelectListItem>()
            {
                new SelectListItem{ Text="01", Value="01"},
                new SelectListItem{ Text="02", Value="02"},
                new SelectListItem{ Text="03", Value="03"},
                new SelectListItem{ Text="04", Value="04"},
                new SelectListItem{ Text="05", Value="05"},
                new SelectListItem{ Text="06", Value="06"},
                new SelectListItem{ Text="07", Value="07"},
                new SelectListItem{ Text="08", Value="08"},
                new SelectListItem{ Text="09", Value="09"},
                new SelectListItem{ Text="10", Value="10"},
                new SelectListItem{ Text="11", Value="11"},
                new SelectListItem{ Text="12", Value="12"},
            };

            return new SelectList(items, "Value", "Text");
        }

        public SelectList GetCreditCardYears()
        {
            int currentYear = DateTime.Now.Year;

            var items = new List<SelectListItem>()
            {
                new SelectListItem{ Text=currentYear.ToString(), Value=currentYear.ToString()},
                new SelectListItem{ Text=(currentYear + 1).ToString(), Value=(currentYear + 1).ToString()},
                new SelectListItem{ Text=(currentYear + 2).ToString(), Value=(currentYear + 2).ToString()},
                new SelectListItem{ Text=(currentYear + 3).ToString(), Value=(currentYear + 3).ToString()},
                new SelectListItem{ Text=(currentYear + 4).ToString(), Value=(currentYear + 4).ToString()},
                new SelectListItem{ Text=(currentYear + 5).ToString(), Value=(currentYear + 5).ToString()},
                new SelectListItem{ Text=(currentYear + 6).ToString(), Value=(currentYear + 6).ToString()},
                new SelectListItem{ Text=(currentYear + 7).ToString(), Value=(currentYear + 7).ToString()},
                new SelectListItem{ Text=(currentYear + 8).ToString(), Value=(currentYear + 8).ToString()},
                new SelectListItem{ Text=(currentYear + 9).ToString(), Value=(currentYear + 9).ToString()},
                new SelectListItem{ Text=(currentYear + 10).ToString(), Value=(currentYear + 10).ToString()},
            };

            return new SelectList(items, "Value", "Text");
        }
    }

    

    //    public class EcentricWebAPI
    //{
    //    private string HPPEndPoint = "https://sandbox.ecentric.co.za/hpp";
    //    private Uri WebApiEndPoint = new Uri("https://sandbox.ecentricswitch.co.za:8443/paymentgateway/v1/");

    //    //NOTES: Step 1: Generate transaction details
    //    //https://sandbox.ecentric.co.za/DevPortal/documentation/#/DevPortal/documentation/getting-started/hpp-quick-start/
    //    //MerchantReference: You will need to generate a unique reference that you will use to identify your customer's transactions.
    //    //This reference must be ALPHANUMERIC, meaning it can only contain letters or numbers (or a combination of both) 
    //    //and must not contain any spaces or special characters. Each transaction must have a new MerchantReference value. 
    //    //For references greater than 8 characters, the first 8 characters of the reference must be unique for each transaction. 
    //    //  Example: Testing123

    //    //Amount: This is the amount that will be paid by your customer.The amount value must be in cents and should not include any decimal points.
    //    //For an Amount of R65.00, you should pass 6500 as the Amount value, see example below;

    //    //Example: 6500
    //    //UserID(Optional) : If you maintain a database of your customers and you have a unique reference to identify them, 
    //    //then you may pass that value here.The value must be ALPHANUMERIC(as explained above). 
    //    //If your system allows for non-Alphanumeric UserID's, then we advise that you generate ALPHANUMERIC UserID's specifically for making calls 
    //    //to the Hosted Payment Page.When you pass a UserID value, the Payment Page will present an option to save the card details for that particular user, 
    //    //so that they do not have to enter the card details again when accessing the payment page.
    //    //The user will only need to enter their CVV number when making a subsequent payment.


    //    //Successful Responses
    //    // MerchantReference: Value generated by the merchant and initially passed in the Request. Usually the Order Number
    //    // Result: Value generated by Ecentric and will be "Success"
    //    //FailureMessage: Value generated by Ecentric, describing the error that occurred.
    //    // Result will be "Success" if no error occurred
    //    // Checksum: Value generated by Ecentric using calculation specified below
    //    //TransactionID: Value generated by Ecentric for the transaction response
    //    //Amount: Value generated by the Merchant and passed during the request
    //    //TransactionDetails: An Array containing the tender details
    //    //Amount: Amount paid in cents
    //    //PaymentService: Value generated by Ecentric, CardNotPresent for Card Payments
    //    //ResponseDetail: Object containing message details
    //    //Source: Value will be "Acquirer" for all Card Payments responses from the Bank
    //    //Code: Value will be "00" for a Successful transaction
    //    //Description: Value will be "Approved or completed successfully" for a Successful tender transaction
    //    //ClientMesage: Value will be "Approved. " for a Successful tender transaction
    //    //TransactionID: Value generated by Ecentric for the tender payment
    //    //TransactionStatus: Value will be "Success" for a Successful tender transaction

    //    //WebAPi examples
    //    //https://sites.google.com/site/wcfpandu/web-api/calling-a-web-api-from-c-and-calling-a-web-api-from-view

    //    public async System.Threading.Tasks.Task<bool> DonationPaymentAsync(Models.Payment payment)
    //    {
    //        //using (var client = new HttpClient())
    //        //{
    //        //    client.BaseAddress = WebApiEndPoint;
    //        //    client.DefaultRequestHeaders.Accept.Clear();
    //        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

    //        //    HttpResponseMessage response = await client.PostAsJsonAsync("Payment", payment);
    //        //    if (response.IsSuccessStatusCode)
    //        //    {
    //        //        // Get the URI of the created resource.
    //        //        //Uri gizmoUrl = response.PaymentResponse.TransactionStatus;

    //        //        //response.Headers
    //        //        var x = 1;
    //        //    }
    //        //    else
    //        //    {
    //        //        var y = 2;
    //        //    }
    //        //}
    //        using (var client = new HttpClient())
    //        {
    //            client.BaseAddress = WebApiEndPoint;

    //            //Called Member default GET All records  
    //            //GetAsync to send a GET request   
    //            // PutAsync to send a PUT request  
    //            var responseTask = client.PutAsync("Payment", new FormUrlEncodedContent(new[]
    //            {
    //                new KeyValuePair<string, string>("Amount", payment.Amount.ToString()),
    //                new KeyValuePair<string, string>("CurrencyCode", payment.CurrencyCode),
    //                new KeyValuePair<string, string>("MerchantID", payment.MerchantID),
    //                new KeyValuePair<string, string>("PaymentService", payment.PaymentService),
    //                new KeyValuePair<string, string>("TransactionID", payment.TransactionID)
    //            }));
    //            responseTask.Wait();

    //            //To store result of web api response.   
    //            var result = responseTask.Result;

    //            //If success received   
    //            if (result.IsSuccessStatusCode)
    //            {
    //                //var readTask = result.Content.ReadAsAsync<IList<MemberViewModel>>();
    //                //readTask.Wait();

    //                //members = readTask.Result;
    //            }
    //            else
    //            {
    //                //Error response received   
    //                //members = Enumerable.Empty<MemberViewModel>();
    //                //ModelState.AddModelError(string.Empty, "Server error try after some time.");
    //            }
    //        }


    //        return true;
    //    }
    //}
}
        

    //private const string URL = "https://sub.domain.com/objects.json";
    //private string urlParameters = "?api_key=123";

    //static void Main(string[] args)
    //{
    //    HttpClient client = new HttpClient();
    //    client.BaseAddress = new Uri(URL);

    //    // Add an Accept header for JSON format.
    //    client.DefaultRequestHeaders.Accept.Add(
    //    new MediaTypeWithQualityHeaderValue("application/json"));

    //    // List data response.
    //    HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
    //    if (response.IsSuccessStatusCode)
    //    {
    //        // Parse the response body.
    //        var dataObjects = response.Content.ReadAsAsync<IEnumerable<DataObject>>().Result;  //Make sure to add a reference to System.Net.Http.Formatting.dll
    //        foreach (var d in dataObjects)
    //        {
    //            Console.WriteLine("{0}", d.Name);
    //        }
    //    }
    //    else
    //    {
    //        Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
    //    }

    //    //Make any other calls using HttpClient here.

    //    //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
    //    client.Dispose();
    //}