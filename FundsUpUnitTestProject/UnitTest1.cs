﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FundsupWebApplication;
using FundsupWebApplication.EcentricPaymentService;

namespace FundsUpUnitTestProject
{
    

    [TestClass]
    public class UnitTest1
    {
        private Uri WebApiEndPoint = new Uri("https://sandbox.ecentricswitch.co.za:8443/paymentgateway/v1/");


        [TestMethod]
        public void TestPaymentMockData()
        {
            using (FundsupWebApplication.EcentricPaymentService.PaymentGatewayServiceClient service = new FundsupWebApplication.EcentricPaymentService.PaymentGatewayServiceClient())
            {
                var payment = service.PaymentAsync(new FundsupWebApplication.EcentricPaymentService.PaymentRequest
                {
                    Amount = 6012,
                    AuthCode = string.Empty,
                    BankAccount = FundsupWebApplication.EcentricPaymentService.BankAccountType.Credit,
                    //BillingAddress= new EcentricPaymentService.Address(),
                    //BudgetPeriod=,
                    Card = new FundsupWebApplication.EcentricPaymentService.BankCard
                    {
                        CardAssociation = "Visa",
                        CardholderName = "MN VAN AARDE",
                        CardNumber = "4548581340538370",
                        CardType = FundsupWebApplication.EcentricPaymentService.BankCardType.Credit,
                        CardTypeSpecified = false,
                        ExpiryMonth = 3,
                        ExpiryMonthSpecified = true,
                        ExpiryYear = 2020,
                        ExpiryYearSpecified = true,
                        SecurityCode = "123",
                        //Token =,
                    },
                    CardAcceptorName = string.Empty,
                    CurrencyCode = "ZAR",
                    //Email =,
                    FirstName = "Michiel",
                    //HomePhone = ,
                    LastName = "van Aarde",
                    MerchantID = "FD85AFDF-46E9-43C2-A9C7-9D38014564A2",
                    //MessageHeader =,
                    //MobilePhone =,
                    OrderNumber = "Order123",
                    //PAResPayload =,
                    PaymentService = FundsupWebApplication.EcentricPaymentService.PaymentServiceType.CardNotPresent,
                    //PreviousTransactionID =,
                    //ReconID =,
                    //SaleReconID =,
                    //ShippingAddress =,
                    //TerminalID =,
                    TransactionDateTime = DateTime.Now,
                    TransactionID = "Testing1234",
                    //WorkPhone =,

                });

                Assert.AreEqual(payment.Result, "1234");


     //           < PaymentResponse xmlns = "http://www.ecentricswitch.co.za/paymentgateway/v1" >
 
     //< TransactionID > C4CF5951 - 575E-4474 - B2F2 - 38F3B769F076 </ TransactionID >
          
     //         < TransactionDateTime > 2012 - 12 - 01T12: 00:00.00 </ TransactionDateTime >
                  
     //                 < ReconID > 02037E97 - 78F2 - 4AC4 - A062 - EA8463D4492A </ ReconID >
                         
     //                        < AuthCode > 813877 </ AuthCode >
                         
     //                        < TransactionStatus > Success </ TransactionStatus >
                         
     //                        < ResponseDetail xmlns: i = "http://www.w3.org/2001/XMLSchema-instance" >
                           
     //                             < Source > Acquirer </ Source >
                           
     //                             < Code > 00 </ Code >
                           
     //                             < Description > Approved or completed successfully </ Description >
                              
     //                                < ClientMessage > Success </ ClientMessage >
                              
     //                             </ ResponseDetail >
                              
     //                           < AuthAmount > 100 </ AuthAmount >
                              
     //                          </ PaymentResponse >
            }
        }
    }
}
